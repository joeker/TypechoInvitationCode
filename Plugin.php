<?php
/**
 * 邀请码注册
 *
 * @package InvitationCode
 * @author 晨曦啊
 * @version 1.2.2
 * @link https://cx-a.com
 *
 * 历史版本
 * version 1.2.2 at 2019-10-8
 * 增加邀请码导出
 * 修复了永久时间却提示邀请码过期的不仔细导致的BUG;QAQ 对不起！
 * version 1.2.1 at 2019-8-1
 * 修复了未设置cgi.fix_pathinfo=1所导致部份没出现邀请码输入框
 * 优化了生成参数设置位置，更加人性化
 * 加入了邀请码有效期（无法兼容以前版本）
 * version 1.2.0 at 2018-12-25
 * 支持Mysql，SQLite，Pgsql三种数据库
 * 可以自己删除邀请码数据库
 * 修正了一些小错误
 * version 1.1.0 at 2018-12-24
 * 支持Mysql和SQLite两种数据库
 * 禁用插件后原邀请码会被保留
 * version 1.0.1 at 2018-12-22
 * 邀请码自动生成
 * 邀请码次数限制
 * 邀请码数量可选择
 * version 0.1.0 at 2018-11-12
 * 不用替换文件
 * 正常返回提示，页面不跳转
 * version 0.0.1 at 2018-11-11
 * 邀请码注册
 *
 */

class InvitationCode_Plugin implements Typecho_Plugin_Interface
{

    /** @var string 提交路由前缀 */
    public static $action = 'invitation-code';

    /** @var string 控制菜单链接 */
    public static $panel  = 'InvitationCode/page/console.php';

    /**
     * 激活插件方法,如果激活失败,直接抛出异常
     *
     * @access public
     * @return void
     * @throws Typecho_Plugin_Exception
     */
    public static function activate()
    {
        InvitationCode_Plugin::Judge_database();
        Typecho_Plugin::factory('admin/footer.php')->end = array('InvitationCode_Plugin', 'add_input');
        Typecho_Plugin::factory('Widget_Register')->register = array('InvitationCode_Plugin', 'render');
        Helper::addAction(self::$action, 'InvitationCode_Action');
        Helper::addPanel(1, self::$panel, '邀请码', '邀请码控制台', 'administrator');
        return _t('开启成功！');
    }

    private static function Judge_database()
    {
        $db= Typecho_Db::get();
        $getAdapterName = $db->getAdapterName();
        if(preg_match('/^M|m?ysql$/',$getAdapterName)){
            return true;
        }elseif(preg_match('/^S|s?Q|q?L|l?ite$/',$getAdapterName)){
            return true;
        }elseif(preg_match('/^P|p*?gsql$/is',$getAdapterName)){
            return true;
        }else{
            throw new Typecho_Plugin_Exception(_t('对不起，使用了不支持的数据库，无法使用此功能'));
        }
    }

    /**
     * 禁用插件方法,如果禁用失败,直接抛出异常
     *
     * @static
     * @access public
     * @return void
     * @throws Typecho_Plugin_Exception
     */
    public static function deactivate()
    {
        Helper::removeAction(self::$action);
        Helper::removePanel(1, self::$panel);
    }

    /**
     * 获取插件配置面板
     *
     * @access public
     * @param Typecho_Widget_Helper_Form $form 配置面板
     * @return void
     */
    public static function config(Typecho_Widget_Helper_Form $form)
    {

        $db= Typecho_Db::get();
        $prefix = $db->getPrefix();
        $query = $db->select('value')->from("{$prefix}options")
            ->where('name = ?', 'allowRegister');
        $register = $db->fetchRow($query);
        $allowRegister=$register['value'] == 0 ? "<span style='color:red;font-size:20px;'>不允许用户进行注册。</span>" : "<span style='color:green;font-size:20px;'>允许用户进行注册。</span>";
        $str='<div >注意：目前基本信息里面'.$allowRegister.'</div>';
        $open = new Typecho_Widget_Helper_Form_Element_Radio('open_cxa', array("on" => "开启", "off" => "关闭"), "on",
            _t('是否开启邀请码注册'), "请尝试进行注册测试，以免不同模板造成误判。{$str}");
        $form->addInput($open);

    }

    /**
     * 个人用户的配置面板
     *
     * @access public
     * @param Typecho_Widget_Helper_Form $form
     * @return void
     */
    public static function personalConfig(Typecho_Widget_Helper_Form $form){}

    /**
     * 插件实现方法
     *
     * @access public
     * @return void
     */
    public static function render($dataStruct)
    {
        $options = Typecho_Widget::widget('Widget_Options');
        $set = $options->plugin('InvitationCode');
        if($set->open_cxa == 'on'){
            $code=$_POST['code_cxa'];

            if (empty($code)) {
              	$error=['code_cxa'=>'未填填写邀请码！'];
              	InvitationCode_Plugin::Tips_cxa($error);
            }

            $db= Typecho_Db::get();
            $prefix = $db->getPrefix();

            $query = $db->select('id', 'code', 'num' ,'duration')->from("{$prefix}invitation_code")
                ->where('code = ?', $code);
            $info = $db->fetchAll($query);
            if($info){
                if($info[0]['duration'] == 0 || $info[0]['duration'] >= time()){
                    if($info[0]['num'] > 1){
                        $num = $info[0]['num'] - 1;
                        $update = $db->update("{$prefix}invitation_code")
                            ->rows(array('num'=>$num))->where('id=?',$info[0]['id']);
                        $db->query($update);
                    }else{
                        $delete = $db->delete("{$prefix}invitation_code")
                            ->where('id = ?', $info[0]['id']);
                        $db->query($delete);
                    }
                    return $dataStruct;
                }else{
                    $delete = $db->delete("{$prefix}invitation_code")
                        ->where('id = ?', $info[0]['id']);
                    $db->query($delete);
                    $error=['code_cxa'=>'邀请码已过期！'];
                    InvitationCode_Plugin::Tips_cxa($error);
                }
            }
            $error=['code_cxa'=>'邀请码错误！'];
            InvitationCode_Plugin::Tips_cxa($error);
        }
        return $dataStruct;
    }
	public static function Tips_cxa($error){
        $notice = is_array($error) ? array_values($error) : array($error);
        Typecho_Cookie::set('__typecho_notice', Json::encode($notice));
        Typecho_Cookie::set('__typecho_notice_type', 'notice');
        Typecho_Cookie::set('__typecho_remember_name', $_POST['name']);
        Typecho_Cookie::set('__typecho_remember_mail', $_POST['mail']);
        InvitationCode_Plugin::goBack_c();
    }
     /**
     * 返回来路
     *
     * @access public
     * @param string $suffix 附加地址
     * @param string $default 默认来路
     */
    public static function goBack_c($suffix = NULL, $default = NULL)
    {
        //获取来源
        $referer = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : '';

        //判断来源
        if (!empty($referer)) {
            // ~ fix Issue 38
            if (!empty($suffix)) {
                $parts = parse_url($referer);
                $myParts = parse_url($suffix);

                if (isset($myParts['fragment'])) {
                    $parts['fragment'] = $myParts['fragment'];
                }

                if (isset($myParts['query'])) {
                    $args = array();
                    if (isset($parts['query'])) {
                        parse_str($parts['query'], $args);
                    }

                    parse_str($myParts['query'], $currentArgs);
                    $args = array_merge($args, $currentArgs);
                    $parts['query'] = http_build_query($args);
                }

                $referer = Typecho_Common::buildUrl($parts);
            }

            InvitationCode_Plugin::redirect_c($referer, false);
        } else if (!empty($default)) {
            InvitationCode_Plugin::redirect_c($default);
        }
        exit;
    }
  
      /**
     * 重定向函数
     *
     * @access public
     * @param string $location 重定向路径
     * @param boolean $isPermanently 是否为永久重定向
     * @return void
     */
    public static function redirect_c($location, $isPermanently = false)
    {
        /** Typecho_Common */
        $location = Typecho_Common::safeUrl($location);

        if ($isPermanently) {
            header('Location: ' . $location, false, 301);
            exit;
        } else {
            header('Location: ' . $location, false, 302);
            exit;
        }
    }

    public static function add_input(){
        $options = Typecho_Widget::widget('Widget_Options');
      	$url=InvitationCode_Plugin::getUrlInfo();
      	if(preg_match('/admin\/register.php/',$url)){
            $set = $options->plugin('InvitationCode');
            if($set->open_cxa == 'on'){
                $ja=<<<a
<script>
var mail=document.getElementById('mail');
var inputs='<p><label for="code_cxa" class="sr-only">邀请码</label><input type="text" id="code_cxa" name="code_cxa" placeholder="邀请码" value="" class="text-l w-100" /></p>';
var p_input = document.createElement("p");
    p_input.id = "code_input";
mail.parentNode.appendChild(p_input);
document.getElementById('code_input').innerHTML=inputs;
</script>
a;
		echo $ja;
            }
        }
    }
    /**
     * 获取当前URL
     */
    public static function getUrlInfo()
    {
        if (PHP_SAPI == 'cli') {
            $url = isset($_SERVER['argv'][1]) ? $_SERVER['argv'][1] : '';
        } elseif (InvitationCode_Plugin::server('HTTP_X_REWRITE_URL')) {
            $url = InvitationCode_Plugin::server('HTTP_X_REWRITE_URL');
        } elseif (InvitationCode_Plugin::server('REQUEST_URI')) {
            $url = InvitationCode_Plugin::server('REQUEST_URI');
        } elseif (InvitationCode_Plugin::server('ORIG_PATH_INFO')) {
            $url = InvitationCode_Plugin::server('ORIG_PATH_INFO') . (!empty(InvitationCode_Plugin::server('QUERY_STRING'))
                    ? '?' . InvitationCode_Plugin::server('QUERY_STRING') : '');
        } else {
            $url = '';
        }
        ;
        return $url;
    }

    /**
     * 获取server参数
     */
    public static function server($name = '', $default = null)
    {
        $server=$_SERVER;
        if (empty($name)) {
            return $server;
        } else {
            $name = strtoupper($name);
        }
        return isset($server[$name]) ? $server[$name] : $default;
    }
}