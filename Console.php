<?php
/**
 * 邀请码注册 控制台
 *
 */
class InvitationCode_Console extends Typecho_Widget
{
    /**
     * 邀请码注册
     * @return Typecho_Widget_Helper_Form
     */
    public function generateCodeForm()
    {
        /** 构建表单 */
        $options = Typecho_Widget::widget('Widget_Options');
        $form = new Typecho_Widget_Helper_Form(Typecho_Common::url('/action/' . InvitationCode_Plugin::$action, $options->index),
            Typecho_Widget_Helper_Form::POST_METHOD);

        /** 生成数量 */
        $num = new Typecho_Widget_Helper_Form_Element_Text('toNum_cxa', NULL, '10',
            _t('邀请码生成数量'), _t('邀请码生成数量,默认为10次。'));
        $num->input->setAttribute('class', 'mini');
        $form->addInput($num->addRule('required', _t('必须填写邀请码生成数量'))
            ->addRule('isInteger', _t('生成数量必须是纯数字')));

        /** 有效期 */
        $duration = new Typecho_Widget_Helper_Form_Element_Text('duration_cxa', NULL, '0',
            _t('邀请码有效期'), _t('邀请码有效期（单位：小时）,默认为0永久，如果为1就是1小时。'));
        $duration->input->setAttribute('class', 'mini');
        $form->addInput($duration->addRule('isInteger', _t('有效期必须是纯数字')));

        /** 前缀 */
        $prefix = new Typecho_Widget_Helper_Form_Element_Text('prefix_cxa', NULL, 'cxa',
            _t('邀请码前缀'), _t('邀请码附带的前缀'));
        $prefix->input->setAttribute('class', 'mini');
        $form->addInput($prefix);

        /** 使用次数 */
        $num = new Typecho_Widget_Helper_Form_Element_Text('num_cxa', NULL, '1',
            _t('邀请码可使用次数'), _t('邀请码可使用次数,默认为1次,最大5000次。'));
        $num->input->setAttribute('class', 'mini');
        $form->addInput($num->addRule('isInteger', _t('使用次数必须是纯数字')));

        /** 动作 */
        $do = new Typecho_Widget_Helper_Form_Element_Hidden('do');
        $form->addInput($do);

        /** 提交按钮 */
        $submit = new Typecho_Widget_Helper_Form_Element_Submit();
        $submit->input->setAttribute('class', 'btn primary');
        $form->addItem($submit);

        /** 设置值 */
        $do->value('generateCode');
        $submit->value('生成邀请码');

        return $form;
    }
}